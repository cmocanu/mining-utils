#!/usr/bin/python

# Usage: ./pool_monitor.py -r <rig_name> -p <pool> -w <wallet> -h <hashrate> -a <pushover_app_token> -u <pushover_user_token>

import sys, argparse, httplib, urllib, urllib2, json, subprocess
from libs import pushover

def getStats(url):
        print "Get Stats from url: " + url

        req = urllib2.Request(url, headers={ 'User-Agent': 'Mozilla/5.0' })
        data = urllib2.urlopen(req).read()

        return json.loads(data)

def sendAlert(pushoverAppToken, pushoverUserToken, poolHashRate, thresholdHashRate, poolName, rigName):
        subject = "Hashrate for " + rigName + " on " + poolName + " is below threshold"
        message = "Hashrate for " + rigName + " on " + poolName + " is " + str(poolHashRate) + " (threshold is " + str(thresholdHashRate) + ")"

        pushover.sendNotification(pushoverAppToken, pushoverUserToken, subject, message)

def sendFailure(pushoverAppToken, pushoverUserToken, errorMessage, rigName):
        subject = "Failure command on " + rigName
        message = errorMessage
    
        pushover.sendNotification(pushoverAppToken, pushoverUserToken, subject, message)

if __name__ == '__main__':
        # parse the passed arguments
        parser = argparse.ArgumentParser()

        parser.add_argument(
                '-r',
                '--rig',
                dest='rig',
                help='Name of the rig to be monitored',
                default="RIG01",
                required=True
        )
        parser.add_argument(
                '-p',
                '--pool',
                dest='pool',
                help='mining pool; supported pools: graftpool.net, graftmine.net, ethermine.org, miningspeed.com, pool.graft.community',
                default="graftpool.net",
                required=True
        )
        parser.add_argument(
                '-w',
                '--wallet',
                dest='wallet',
                help='wallet address; used for URL construction',
                required=True
        )
        parser.add_argument(
                '-t',
                '--threshold',
                type=int,
                dest='threshold',
                help='hashrate threshold; if below then trigger notification',
                required=True
        )
        parser.add_argument(
                '-a',
                '--pushoverAppToken',
                dest='appToken',
                help='Pushover Application Token',
                required=True
        )
        parser.add_argument(
                '-u',
                '--pushoverUserToken',
                dest='userToken',
                help='Pushover User Token',
                required=True
        )
        parser.add_argument(
                '-c',
                '--command',
                dest='command',
                help='Commannd to run if hashrate is below threshold',
                default=None,
                required=False
        )
        args = parser.parse_args()


        ##########################
        # Main Program
        ##########################

        hashrate = None
        if args.pool == 'graftpool.net':
                from libs import graftpool as poolStats
        elif args.pool == 'graftmine.net':
                from libs import graftmine as poolStats
        elif args.pool == 'ethermine.org':
                from libs import ethermine as poolStats
        elif args.pool == 'miningspeed.com':
                from libs import miningspeed as poolStats
        elif args.pool == 'nicehash.com':
                from libs import nicehash as poolStats
        elif args.pool == 'pool.graft.community':
                from libs import poolgraft as poolStats
        else:
                print "Pool " + args.pool + " not supported!. Please see help (--help argument) for supported pools"
                sys.exit(-1)

        stats = getStats(poolStats.buildStatsUrl(args.rig, args.wallet))
        hashrate = poolStats.getHashrate(args.rig, stats)

        print "Curent Hashrate", hashrate, "threshold is", args.threshold

        if hashrate < args.threshold:
                sendAlert(args.appToken, args.userToken, hashrate, args.threshold, args.pool, args.rig)
                # run command
                if args.command is not None:
                        cmd = args.command.split()
                        try:
                                print ("Run command " + args.command)
                                output = subprocess.check_output(cmd)
                        except subprocess.CalledProcessError:
                                sendFailure(args.appToken, args.userToken, 'Cannot run command', args.command )
