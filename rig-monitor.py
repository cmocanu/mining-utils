#!/usr/bin/python3

# Usage: ./rig_monitor.py -r <rig_name> -g <amd|nvidia> -t <threshold> -a <pushover_app_token> -u <pushover_user_token> -c <command>

import platform, argparse, tempfile, os, subprocess
from libs import pushover

def sendAlert(pushoverAppToken, pushoverUserToken, rigUsage, thresholdRigUsage, rigName):
	subject = "Usage for " + rigName  + " is below threshold"
	message = "Actual usage for " + rigName +  " is " + str(round(rigUsage,2)) + " (threshold is " + str(thresholdRigUsage) + ")"

	pushover.sendNotification(pushoverAppToken, pushoverUserToken, subject, message)

def sendFailure(pushoverAppToken, pushoverUserToken, errorMessage, rigName):
	subject = "Failure command on " + rigName
	message = errorMessage
    
	pushover.sendNotification(pushoverAppToken, pushoverUserToken, subject, message)

if __name__ == '__main__':
	# parse the passed arguments
	parser = argparse.ArgumentParser()

	parser.add_argument(
	        '-r',
	        '--rig',
	        dest='rig',
	        help='Name of the rig to be monitored',
	        default=platform.node(),
	        required=False
	)
	parser.add_argument(
	        '-g',
	        '--gpuType',
	        dest='gpuType',
	        help='GPU Type; allowed values: nvidia and amd (!!! amd is not implemented yet !!!)',
	        default="nvidia",
	        required=False
	)
	parser.add_argument(
	        '-t',
	        '--threshold',
	        type=int,
	        dest='threshold',
	        help='usage threshold; if below then trigger notification and command',
	        default=95,
	        required=False
	)
	parser.add_argument(
	        '-a',
	        '--pushoverAppToken',
	        dest='appToken',
	        help='Pushover Application Token',
	        required=True
	)
	parser.add_argument(
	        '-u',
	        '--pushoverUserToken',
	        dest='userToken',
	        help='Pushover User Token',
	        required=True
	)
	parser.add_argument(
	        '-c',
	        '--command',
	        dest='command',
	        help='What command to be issued if usage is below threshold',
	        default="sudo shutdown -fr now",
	        required=False
	)
	args = parser.parse_args()

	##########################
	# Main Program
	##########################
	if args.gpuType == 'nvidia':
		from libs import nvidia as gpu
	elif args.gpuType == 'amd':
		from libs import amd as gpu
	else:
		print ("GPU Type (", args.gpuType," not supported!. Please see help (--help argument) for supported gpu type")
		sys.exit(-1)

	actualUsage = gpu.getUsage()
	print ("Current GPUs usage is ", str(round(actualUsage,2)))

	checkFile = os.path.join(tempfile.gettempdir(),'rig-monitor')

	if actualUsage < args.threshold:
		if os.path.isfile(checkFile):
			# first time when we detect that usage is less that threshold
			# need to execute the command
			# but first ... notify the boss
			os.remove(checkFile)

			sendAlert(args.appToken, args.userToken, actualUsage, args.threshold, args.rig)

			cmd = args.command.split()
			try:
				print ("Reboot Rig")
				output = subprocess.check_output(cmd)
			except subprocess.CalledProcessError:
				sendFailure(args.appToken, args.userToken, 'Cannot restart the rig')
		else:
			# first time when we detect that usage is less that threshold
			with open(checkFile, 'w'):
				pass
	else:
		# need to erase our previous tracks
		if os.path.isfile(checkFile):
			os.remove(checkFile)
