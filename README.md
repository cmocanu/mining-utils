## rig-monitor.py
```
rig-monitor.py [-h] [-r RIG] [-g GPUTYPE] [-t THRESHOLD] -a APPTOKEN -u
                      USERTOKEN [-c COMMAND]

optional arguments:
  -h, --help            show this help message and exit
  -r RIG, --rig RIG     Name of the rig to be monitored
  -g GPUTYPE, --gpuType GPUTYPE
                        GPU Type; allowed values: nvidia and amd (!!! amd is
                        not implemented yet !!!)
  -t THRESHOLD, --threshold THRESHOLD
                        usage threshold; if below then trigger notification
                        and command
  -a APPTOKEN, --pushoverAppToken APPTOKEN
                        Pushover Application Token
  -u USERTOKEN, --pushoverUserToken USERTOKEN
                        Pushover User Token
  -c COMMAND, --command COMMAND
                        What command to be issued if usage is below threshold
```
## pool_monitoy.py
```
pool_monitor.py [-h] -r RIG -p POOL -w WALLET -t THRESHOLD -a APPTOKEN
                       -u USERTOKEN [-c COMMAND]

optional arguments:
  -h, --help            show this help message and exit
  -r RIG, --rig RIG     Name of the rig to be monitored
  -p POOL, --pool POOL  mining pool; supported pools: graftpool.net,
                        graftmine.net, ethermine.org, miningspeed.com,
                        pool.graft.community
  -w WALLET, --wallet WALLET
                        wallet address; used for URL construction
  -t THRESHOLD, --threshold THRESHOLD
                        hashrate threshold; if below then trigger notification
  -a APPTOKEN, --pushoverAppToken APPTOKEN
                        Pushover Application Token
  -u USERTOKEN, --pushoverUserToken USERTOKEN
                        Pushover User Token
  -c COMMAND, --command COMMAND
                        Commannd to run if hashrate is below threshold
```

