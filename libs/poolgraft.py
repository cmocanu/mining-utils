# graftpool.net specific functions

def buildStatsUrl(rig, wallet):
	return "https://pool.graft.community/api/stats_address?address=" + wallet + "&longpoll=false"

def getHashrate(rig, stats):
	hashrate = None

	for key, value in stats.iteritems():
		if key == "workers":
			for woker in value:
				if woker["name"] == rig:
					hashrate = round(float(woker["hashrate"]) / 1000, 2)
	
	return hashrate