# nvidia related functions
# needs python3

import subprocess, re

def getUsage():
	cmd = ['/usr/bin/nvidia-smi', '--query-gpu=utilization.gpu', '--format=csv']
	try:
		output = subprocess.check_output(cmd, timeout=120)
	except subprocess.CalledProcessError:
		return -1
	except subprocess.TimeoutExpired:
		return -100

	sum = 0
	if output != '':
		values = re.findall(r'\b\d+\b', output.decode('utf-8'))
		for value in values:
			sum += float(value)
		
		return sum / len(values)

	return 0

