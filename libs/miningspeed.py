# miningspeed.com specific functions
import json
def buildStatsUrl(rig, wallet):
	return "https://hush.miningspeed.com/api/worker_stats?" + wallet

def getHashrate(rig, stats):
	hashrate = None

	if stats.get("workers"):
		workers = stats["workers"]

		for worker in workers:
			workerStats = workers[worker]
			if workerStats.get("name") and workerStats.get("hashrateString"):
				if workerStats["name"].endswith("." + rig):
					hashrate = workerStats["hashrateString"][:workerStats["hashrateString"].index(" ")]
					hashrate = round(float(hashrate), 2)
	
	return hashrate
