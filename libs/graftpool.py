# graftpool.net specific functions

def buildStatsUrl(rig, wallet):
	return "http://graftpool.net/api/miner/" + wallet + "/stats/allWorkers"

def getHashrate(rig, stats):
	hashrate = None

	for key, value in stats.iteritems():
		if value.get("identifer") and value.get("hash"):
			if value["identifer"] == rig:
				hashrate = round(float(value["hash"]) / 1000, 2)
	
	return hashrate