# ethermine.org specific functions

def buildStatsUrl(rig, wallet):
	return "https://api.ethermine.org/miner/" + wallet + "/dashboard"

def getHashrate(rig, stats):
	hashrate = None

	if stats.get("data"):
		if stats["data"].get("workers"):
			workers = stats["data"]["workers"]
			for worker in workers:
				if worker.get("worker") and worker.get("reportedHashrate"):
					if worker["worker"] == rig:
						hashrate = worker["reportedHashrate"]
						hashrate = round(float(hashrate) / 1000000, 2)
	
	return hashrate
