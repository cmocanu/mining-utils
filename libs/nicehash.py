# ethermine.org specific functions

def buildStatsUrl(rig, wallet):
	return "https://api.nicehash.com/api?method=stats.provider.workers&addr=" +wallet

def getHashrate(rig, stats):
	hashrate = None

	if stats.get("result"):
		if stats["result"].get("workers"):
			workers = stats["result"]["workers"]
			for worker in workers:
				if worker[0]:
					if worker[0] == rig:
						if worker[1].get("a"):
							hashrate = worker[1]["a"]
	
	return hashrate
