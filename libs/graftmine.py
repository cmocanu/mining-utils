# graftmine.net specific functions

def buildStatsUrl(rig, wallet):
	return "https://www.graftmine.net/api/stats_address?address=" + wallet + "&longpoll=true"

def getHashrate(rig, stats):
	hashrate = None

	for item in stats:
		if item.get("name") and item.get("stats"):
			if item["name"] == rig:
				if item["stats"].get("hashrate"):
					hashrate = round(float(item["stats"]["hashrate"]) / 1000, 2)
	
	return hashrate