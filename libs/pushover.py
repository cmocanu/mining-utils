# pushOver send message function
# needs python3

import requests
 
def sendNotification(appToken, userToken, subject, message):
    data = {
        'user': userToken,
        'token': appToken,
        'title': subject,
        'message': message
    }

    response = requests.post('https://api.pushover.net/1/messages.json', data=data)
    if response.status_code != 200:
        return False

    if response.json()['status'] == 1:
        return True

    return False
